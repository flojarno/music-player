public class Player {
    public static void main(String[] args) {

        Artist artist1 = new Artist("Bob", "Marley");
        Music song1 = new Music("Redemption song", 180, new Artist[]{ artist1 });
        Artist artist2 = new Artist("John", "Coltrane");
        Music song2 = new Music("Blue train", 600, new Artist[]{ artist2 });
        Music song3 = new Music("Redemption train", 8400, new Artist[] { artist1, artist2 });

        artist1.getFullName();
        song1.getInfos();
        Playlist playlist = new Playlist();
        playlist.add(song1);
        playlist.add(song2);
        playlist.add(song3);
        playlist.remove(2);
        playlist.getTotalDuration();
        playlist.next();
    }
}