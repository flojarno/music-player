import java.util.ArrayList;

public class Music {

    private String title;
    private int duration;
    private Artist[] artistSet;    // composition = une classe contient des instances d'une autre classe, mais sans relation d'héritage

    public Music(String title, int duration, Artist[] artistSet){
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    public String getInfos() {

        int minutes = this.duration / 60;
        int seconds = this.duration % 60;

        // pattern mm:ss avec String.format()
        // % : spécificateur de format
        // 0 : rempli avec des zéros à gauche si la sortie est moins longue que le format prévu
        // 2 : le format prévu, 2 chiffres
        // d : spéficie le type de l'argument que l'on passe dans la méthode format() : des entiers décimaux
        String formattedDuration = String.format("%02d:%02d", minutes, seconds);

        ArrayList<String> artistNames = new ArrayList<>();
        for (Artist artist : this.artistSet){
            artistNames.add(artist.getFullName());
        }

        // String.join() : prend un tableau, list, set en argument, et un délimiteur, et sort une string
        return this.title + " - " + formattedDuration + " par " + String.join(", ", artistNames);
    }

    public int getDuration() {
        return this.duration;
    }
}




