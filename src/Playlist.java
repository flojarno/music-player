import java.util.ArrayList;

public class Playlist {

    private Music currentMusic; //peut être nulle
    private ArrayList<Music> musicList; // une même musique peut être ajoutée plusieurs fois

    public Playlist() {
        this.currentMusic = null;
        this.musicList = new ArrayList<>();
    }


    public void add(Music music) {
       this.musicList.add(music);
    }

    public void remove(int position) {
        this.musicList.remove(position);
    }

    public String getTotalDuration() {

        int totalDuration = 0;
        for (Music music: this.musicList) {
            totalDuration += music.getDuration();
        }

        int hours = totalDuration / 3600;
        int minutes = (totalDuration % 3600) / 60;
        int seconds = totalDuration % 60;

        // Format hh:mm:ss
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    //passe à la musique suivante dans la liste et la définit comme musique en cours
    public void next() {
        if (!this.musicList.isEmpty()) {

            // si curretnMusic est null, on la définit comme première musique de la liste
            if (this.currentMusic == null) {
                this.currentMusic = this.musicList.get(0);
            // sinon on récupère l'index de la currentMusic avec ArrayList.indexOf()
            } else {
                int currentIndex = this.musicList.indexOf(this.currentMusic);

                // on définit l'index suivant
                int nextIndex = currentIndex + 1;

                // si l'index suivant est >= au nombre d'éléments de la liste, on revient au début de la liste
                if (nextIndex >= this.musicList.size()) {
                    nextIndex = 0;
                }
                // on définit currentMusic comme la musique a l'index suivant
                this.currentMusic = this.musicList.get(nextIndex);
            }
        }
    }
}
