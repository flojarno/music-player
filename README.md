# Java - Gestion d'une Playlist Musicale

Ce projet est une introduction aux bases du langage Java à travers un programme simple de gestion de playlists musicales. Le programme permet de stocker des musiques, de naviguer entre les musiques d'une playlist, et d'afficher les informations des musiques actuellement sélectionnées.

## Fonctionnalités

- **Navigation dans la Playlist** : Permet de passer à la musique suivante ou précédente et gère le passage de la dernière à la première musique de manière circulaire.
- **Affichage des Informations Musicales** : Affiche le titre, la durée et les artistes de la musique actuellement en lecture.
- **Gestion Dynamique** : Ajoute et retire des musiques à la playlist en temps réel.

## Démarrage Rapide

1. **Clonez le dépôt** :
   ```bash
   git clone git@gitlab.com:flojarno/music-player.git
    ```
2. Ouvrez le projet dans votre IDE 

3. Compilez et exécutez le programme
- Naviguez dans le dossier racine du projet.
- Compilez le fichier source :
   ```bash
  javac Main.java
   ```
- Exécutez le programme compilé :
     ```bash
  java Main
   ```